<?php
/**
 * Plugin Name:       Print Functionality for MailPoet Newsletter
 * Plugin URI:        https://gitlab.com/brendon-scott/print-functionality-to-mailpoet-newsletter
 * Description:       Allows Print Functionailty for MailPoet Newsletter
 * Version:           1.0
 * Author:            Rehan Khan
 * Author URI:        https://
 * Text Domain:       fakerpress
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

add_filter( 'mailpoet_sending_newsletter_render_after_pre_process', 'bs_mailpoet_newsletter_print_functionality', 10 );

/**
 * Pulls the data in HTML Format, replaces elements based on our preferences, adds our own custom styles,
 * Publishes a post with the modified content, retrieves it's url and adds it to the Newsletter.
 *
 * @param [type] $data
 * @return void
 */
function bs_mailpoet_newsletter_print_functionality( $data ) {

	$content = $data['html'];

	// Add our own styling.
	$content = str_replace( '<table', '<table style="padding: 0px; border-color:transparent;"', $content );

	$content = str_replace( '<td', '<td style="padding: 0px; border-color:transparent;"', $content );

	$content = str_replace( '<button', '<button style="display:none"', $content );

		$content = str_replace( 'Share This Newsletter:', '', $content );

		$content = str_replace( 'Unsubscribe', '', $content );

		$content = str_replace( 'Subscribe', '', $content );

		$content = str_replace( 'Manage your subscription', '', $content );

		$content = str_replace( "ST. JOHN'S REPORTER", '', $content );
		$content = str_replace( '|', '', $content );
		$content = str_replace( '[bws-link]', '', $content );
		$content = str_replace(
			'<td style="padding: 0px; border-color:transparent;" class="mailpoet_content-cols-two" align="left" style="border-collapse:collapse;background-color:#2e6dbd!important" bgcolor="#2e6dbd">',
			'<td style="padding: 0px; border-color:transparent;" class="mailpoet_image mailpoet_padded_vertical mailpoet_padded_side" align="center" valign="top">
          <a href="" style="color:#5d6873;text-decoration:none"><img src="' . plugins_url( 'assets/NewsletterPrintHeader.png', __FILE__ ) . '" alt="" style="height:auto;
	max - width:100 % ;
	- ms - interpolation - mode:bicubic;
	border:0;
	display:block;
	outline:none;text - align:center"></a>
        </td><td style="padding: 0px;
	border - color:transparent;
	display:none;
	" class="mailpoet_content - cols - two" align="left" style="border - collapse:collapse;background - color:// 2e6dbd!important" bgcolor="#2e6dbd">',
			$content
		);

		$content = str_replace( 'img/mailpoet_logo_newsletter.png"', 'img/mailpoet_logo_newsletter.png" style="display:none !important;"', $content );
		$content = str_replace( '12/SJLCNameplateWhite-1024x554.png"', '12/SJLCNameplateWhite-1024x554.png" style="display:none !important;"', $content );
		$style   = '';
		$style  .= '<style>*{background-color:#fff;color:#000!important;border-color:transparent!important;font-family:"Open Sans","HelveticaNeue - Light","Helvetica Neue Light","Helvetica Neue",Helvetica,"sans-serif"}</style>';
		$style  .= '<style>.mailpoet_spacer,a[href="https://www.mailpoet.com/"]{display:none}</style>';
		$style  .= '<style>div#content{min-width:90%!important;display:flex;align-items:center;justify-content:center;text-align:left!important}</style>';

		$style .= '<style>.mailpoet_button{display:none!important}</style>';
		$style .= '<style>.mailpoet_paragraph {text-align: left;}</style>';
		$style .= '<style>.mailpoet_button-container{display:none !important ;}</style>';
		$style .= '<style>.mailpoet_padded_side mailpoet_padded_vertical{display:none !important ;}</style>';
		$style .= '<style>.entry-content > p {display:none;}</style>';
		$style .= '<style>.postmetadata {display:none;}</style>';
		$style .= '<style>.mailpoet_preheader {display:none;}</style>';
		$style .= '</head>';

		$content = str_replace( '</head>', $style, $content );
		$content = str_replace( '[bws-link]', '', $content );

	$my_post = array(
		'post_title'   => 'My post' . time(),
		'post_content' => $content,
		'post_status'  => 'publish',
		'post_author'  => 1,
		'tax_input '   => array( 'category' => array( 1 ) ),
	);

	// Insert the post into the database.

	kses_remove_filters();

	$post_id = wp_insert_post( $my_post );
	kses_init_filters();

	if ( is_wp_error( $post_id ) ) {
		return $data;
	}
	wp_set_post_terms( $post_id, array( 1 ), 'category' );

	// Retrieve Link.
	$link = get_permalink( $post_id );

	$data['html'] = str_replace( '[bws-link]', '<div class="pdfprnt-buttons pdfprnt-buttons-post pdfprnt-top-right"><a href="' . $link . '?print=print" class="pdfprnt-button pdfprnt-button-print" target="_blank"><img decoding="async" class=" lazyloaded" src="https://stjohnslaporte.org/wp-content/plugins/pdf-print/images/print.png" data-src="https://stjohnslaporte.org/wp-content/plugins/pdf-print/images/print.png" alt="image_print" title="Print Content"></a></div>', $data['html'] );

	return $data;
}
