# Print Functionality to Mailpoet Newsletter


The Objective of this customization is to add a print functionality to MailPoet Newsletter. 

The Overall Process is the following:- 

We hook into the `mailpoet_sending_newsletter_render_after_pre_process` hook so we can modify the newsletter content before it's send to the users. We will pull the data in the HTML format, next we will add our own custom styling to it. After our changes have been added to the Newsletter HTML we will publish it as a post, next we will retrieve its url and add it as a link in the newsletter that is being sent to the users by replacing the following piece of text '[bws-link]';